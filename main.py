from fastapi import FastAPI, Depends, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from sqlalchemy.orm import Session

from todo.database import engine, get_session
from todo.model import todo_list
from todo.config import settings

from todo.api import router


app = FastAPI()

todo_list.Base.metadata.create_all(engine)
app.include_router(router)

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


@app.get("/")
async def home(request: Request, session: Session = Depends(get_session)):
    todos = session.query(todo_list.ToDoModel).all()

    return templates.TemplateResponse(
        "index.html",
        {
            "request": request,
            "app_name": settings.app_name,
            "todo_list": todos,
        },
    )
