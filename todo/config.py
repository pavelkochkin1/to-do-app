from pydantic import BaseSettings


class Settings(BaseSettings):
    db_url: str = "sqlite:///./sql_app.db"
    app_name: str = "ToDo List"


settings = Settings()
