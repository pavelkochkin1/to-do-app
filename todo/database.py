from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .config import settings


engine = create_engine(
    settings.db_url,
    connect_args={"check_same_thread": False},
)


Session = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine,
)


def get_session() -> sessionmaker:
    session = Session()
    try:
        yield session
    finally:
        session.close()
