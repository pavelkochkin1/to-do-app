from fastapi import APIRouter, Request
from fastapi import Depends, Form
from fastapi.responses import RedirectResponse
from starlette import status as st

from sqlalchemy.orm import Session

from todo.database import get_session
from todo.model import todo_list

router = APIRouter()


@router.post("/add", tags=["todo"])
async def add(
    request: Request,
    title: str = Form(...),
    session: Session = Depends(get_session),
):
    new_todo = todo_list.ToDoModel(title=title)

    session.add(new_todo)
    session.commit()
    session.refresh(new_todo)

    url = request.url_for("home")

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)


@router.get("/update/{todo_id}", tags=["todo"])
async def update(
    request: Request,
    todo_id: int,
    session: Session = Depends(get_session),
):
    todo: todo_list.ToDoModel = (
        session.query(todo_list.ToDoModel).filter_by(id=todo_id).first()
    )

    todo.is_complete = not todo.is_complete
    session.commit()

    url = request.url_for("home")

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)


@router.get("/delete/{todo_id}", tags=["todo"])
async def delete(
    request: Request,
    todo_id: int,
    session: Session = Depends(get_session),
):
    todo = session.query(todo_list.ToDoModel).filter_by(id=todo_id).first()

    session.delete(todo)
    session.commit()

    url = request.url_for("home")

    return RedirectResponse(url=url, status_code=st.HTTP_303_SEE_OTHER)
