from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa
from pydantic import BaseModel


Base = declarative_base()


class ToDoModel(Base):
    __tablename__ = "todos"

    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String, unique=True)
    is_complete = sa.Column(sa.Boolean, default=False)


class ToDoSchema(BaseModel):
    id: int
    title: str
    is_complete: bool

    class Config:
        orm_mode = True
