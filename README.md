# To-Do Web-app для собственного использования.
Список дел.
### Как запустить?
1. Установить менеджер пакетов `pip install poetry`
2. Установить зависимости `poetry install --no-dev`
3. Запустить сервис `source local_run.sh`

### Пример:
![Usage example](img/todo_example.gif)